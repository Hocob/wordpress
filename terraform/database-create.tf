module "db" {
  source            = "terraform-aws-modules/rds/aws"
  identifier        = "${local.cluster}"
  engine            = "mysql"
  engine_version    = "5.7.19"
  instance_class    = "db.t2.micro"
  allocated_storage = 5
  storage_encrypted = false

  # kms_key_id        = "arm:aws:kms:<region>:<accound id>:key/<kms key id>"
  name                    = "${local.cluster}"
  username                = "${local.cluster}"
  password                = "ZCZ6PErM5K3Wkld9XY2d"
  port                    = "3306"
  vpc_security_group_ids  = ["${aws_security_group.db.id}"]
  multi_az                = true
  backup_retention_period = 0

  tags = {
    Environment = "${local.cluster}"
  }

  subnet_ids                = "${module.vpc.public_subnets}"
  family                    = "mysql5.7"
  major_engine_version      = "5.7"
  final_snapshot_identifier = "${local.cluster}"
  deletion_protection       = false
  maintenance_window        = "Mon:00:00-Mon:01:00"
  backup_window             = "03:00-04:00"
}

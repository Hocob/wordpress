---

- name: Install role dependencies
  apt:
    name: "{{ docker__package_dependencies }}"

- name: Add Docker's GPG key
  apt_key:
    id: "{{ docker__apt_key_id }}"
    url: "{{ docker__apt_key_server }}"

- name: Configure Docker's APT repository
  apt_repository:
    repo: "{{ docker__apt_repository }}"
    update_cache: true

- name: Install Docker
  apt:
    name: "docker-{{ docker__edition }}"
  register: _docker_installed

- name: Install Docker Compose
  get_url:
    url: "{{ docker__compose_download_url }}"
    dest: "/usr/local/bin/docker-compose"
    force: true
    owner: "root"
    group: "root"
    mode: "0755"
  register: _docker_compose_arrived

- name: Docker-compose systemd file
  template:
    src: "templates/docker-compose-app.service.j2"
    dest: "/etc/systemd/system/docker-compose-app.service"
  notify: ["Restart docker-compose"]
  register: _docker_compose_systemd_file_arrived

- name: Enable docker-compose-app service
  command: "systemctl enable docker-compose-app"
  when: _docker_compose_systemd_file_arrived is changed and _docker_compose_arrived and _docker_installed
  register: _docker_compose_service_enabled

- name: Install NFS mount utility
  become: yes
  apt:
    pkg: nfs-common
    state: present

- name: Mount NFS share
  become: yes
  mount:
    fstype: nfs4
    name: "{{ nfs_name }}"
    opts: "{{ nfs_mount_opts }}"
    src: "{{ nfs__mountpoint }}:/"
    state: mounted

- name: Creating base dir
  file:
    path: "{{ base__dir }}"
    state: directory
    mode: 0755

- name: Generating nginx.conf
  template:
    src: "templates/nginx.conf.j2"
    dest: "{{ base__dir }}/nginx.conf"
  notify: ["Restart docker-compose"]

- name: Generating docker-compose file
  template:
    src: "templates/docker-compose.yml.j2"
    dest: "{{ base__dir }}/docker-compose.yml"
  notify: ["Restart docker-compose"]

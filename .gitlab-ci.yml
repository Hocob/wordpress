stages:
  - build
  - container_scanning
  - terraform:plan
  - terraform:deploy
  - ansible
  - terraform:destroy
  
variables:
  APP_IMAGE: "${CI_REGISTRY_IMAGE}:${CI_PIPELINE_ID}"
  AWS_REGION: us-east-1
  AWS_S3_BUCKET: tempstate

build:docker:app:
  stage: build
  image: docker:latest
  services:
  - docker:dind
  script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker build -t "${APP_IMAGE}" -f "./bedrock/Dockerfile" "./bedrock"
    - docker push "${APP_IMAGE}"

container_scanning:
  stage: build
  image: docker:stable
  variables:
    DOCKER_DRIVER: overlay2
    ## Define two new variables based on GitLab's CI/CD predefined variables
    ## https://docs.gitlab.com/ee/ci/variables/#predefined-environment-variables
    CI_APPLICATION_REPOSITORY: $APP_IMAGE
    CI_APPLICATION_TAG: $CI_COMMIT_SHA
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - docker run -d --name db arminc/clair-db:latest
    - docker run -p 6060:6060 --link db:postgres -d --name clair --restart on-failure arminc/clair-local-scan:v2.0.6
    - apk add -U wget ca-certificates
    - docker pull ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG}
    - wget https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64
    - mv clair-scanner_linux_amd64 clair-scanner
    - chmod +x clair-scanner
    - touch clair-whitelist.yml
    - while( ! wget -q -O /dev/null http://docker:6060/v1/namespaces ) ; do sleep 1 ; done
    - retries=0
    - echo "Waiting for clair daemon to start"
    - while( ! wget -T 10 -q -O /dev/null http://docker:6060/v1/namespaces ) ; do sleep 1 ; echo -n "." ; if [ $retries -eq 10 ] ; then echo " Timeout, aborting." ; exit 1 ; fi ; retries=$(($retries+1)) ; done
    - ./clair-scanner -c http://docker:6060 --ip $(hostname -i) -r gl-container-scanning-report.json -l clair.log -w clair-whitelist.yml ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG} || true
  artifacts:
    reports:
      container_scanning: gl-container-scanning-report.json
      
terraform:plan:
  stage: terraform:plan
  image:
    name: hashicorp/terraform:0.11.9
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  artifacts:
    paths:
    - terraform/plan.bin
    expire_in: 1 week
  variables:
    TF_VAR_aws_access_key: "${AWS_ACCESS_KEY}"
    TF_VAR_aws_secret_key: "${AWS_SECRET_KEY}"
    TF_VAR_public_key: "${SSH_PUBLIC_KEY}"
  before_script:
    - cd terraform
  script:
    - terraform init -backend-config="bucket=${AWS_S3_BUCKET}"
      -backend-config="region=us-east-1"
      -backend-config="access_key=${AWS_ACCESS_KEY}"
      -backend-config="secret_key=${AWS_SECRET_KEY}"
      -backend-config="session_name=TerraformBackend"
    - terraform plan -input=false -out=plan.bin

terraform:deploy:
  stage: terraform:deploy
  dependencies:
    - terraform:plan
  image:
    name: hashicorp/terraform:0.11.9
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  artifacts:
    paths:
    - terraform/tfstate.bin
    expire_in: 1 week
  variables:
    TF_VAR_aws_access_key: "${AWS_ACCESS_KEY}"
    TF_VAR_aws_secret_key: "${AWS_SECRET_KEY}"
    TF_VAR_public_key: "${SSH_PUBLIC_KEY}"
  before_script:
    - cd terraform
  script:
    - terraform init -backend-config="bucket=${AWS_S3_BUCKET}"
      -backend-config="region=us-east-1"
      -backend-config="access_key=${AWS_ACCESS_KEY}"
      -backend-config="secret_key=${AWS_SECRET_KEY}"
      -backend-config="session_name=TerraformBackend"
    - terraform apply -auto-approve -input=false plan.bin
    - terraform state pull > tfstate.bin

ansible:
  stage: ansible
  dependencies:
    - terraform:deploy
    - build:docker:app
  image:
    name: philm/ansible_playbook
    entrypoint:
      - '/usr/bin/env'
  variables:
    ANSIBLE_HOST_KEY_CHECKING: "false"
    ADMIN_USER: "${ADMIN_USER}"
    ADMIN_PASS: "${ADMIN_PASS}"
    IMAGE_NAME: "${CI_REGISTRY_IMAGE}:${CI_PIPELINE_ID}"
  before_script:
    - cd ansible
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
  script:
    - ansible-playbook inventory_generate.yaml
    - ansible-playbook -i ./terraform.inv ec2_instance_configure.yaml

terraform:destroy:
  stage: terraform:destroy
  image:
    name: hashicorp/terraform:0.11.9
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  artifacts:
    paths:
    - terraform/plan.bin
    expire_in: 1 week
  variables:
    TF_VAR_aws_access_key: "${AWS_ACCESS_KEY}"
    TF_VAR_aws_secret_key: "${AWS_SECRET_KEY}"
    TF_VAR_public_key: "${SSH_PUBLIC_KEY}"
  before_script:
    - cd terraform
  script:
    - terraform init -backend-config="bucket=${AWS_S3_BUCKET}"
      -backend-config="region=us-east-1"
      -backend-config="access_key=${AWS_ACCESS_KEY}"
      -backend-config="secret_key=${AWS_SECRET_KEY}"
      -backend-config="session_name=TerraformBackend"
    - terraform destroy -input=false -auto-approve
  when: manual


